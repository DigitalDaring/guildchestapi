import * as path from "path";
import express from "express";
import * as bodyParser from "body-parser";
import DbAuth from "./dbauth/index";
import {UserModel} from "./models/user";
import {AuthRequestRoutes} from "./routes/auth-request";
import {UsersRoutes} from "./routes/users";
import * as mongodb from "mongodb";
import {ChestsRoutes} from "./routes/chests";
import {ProfilesRoutes} from "./routes/profiles";
import {KeysRoutes} from "./routes/keys";

class App {

    express: express.Application;

    private enableCors(): void {
        this.express.use(function(req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
            next();
        });
    }

    private middleware(): void {
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({extended: false}));
    }

    private routes(): void {
        const router = express.Router();

        router.get("/", (req, res, next) => {
            res.json({
                message: 'Hello World'
            });
        });

        router.get("/validated", async (req, res, next) => {
            const authToken = req.headers.Authorization;
            if (authToken) {
                const foundUser = await DbAuth.confirmKey(authToken.toString());
                if (foundUser !== null) {
                    res.status(200).json({
                        "message": `Welcome, ${foundUser.email}!`
                    });
                }
            }
            res.status(401).send();

        });

        const authRoutes = new AuthRequestRoutes();
        authRoutes.registerRoutes(router);

        const userRoutes = new UsersRoutes();
        userRoutes.registerRoutes(router);

        const chestsRoutes = new ChestsRoutes();
        chestsRoutes.registerRoutes(router);

        const profilesRoutes = new ProfilesRoutes();
        profilesRoutes.registerRoutes(router);

        const keysRoutes = new KeysRoutes();
        keysRoutes.registerRoutes(router);

        this.express.use('/', router);
    }

    constructor() {
        this.express = express();
        this.enableCors();
        this.middleware();
        this.routes();
    }
}

export default new App().express;