import {UserModel} from "../models/user";
import * as crypto from 'crypto';

class AuthCache {
    private keyMap;

    constructor(){
        this.keyMap = {};
    }

    async registerNewKey(user: UserModel): Promise<string>{
        const generatedKey = await this.createRandomKey();
        this.keyMap[generatedKey] = user;
        return Promise.resolve(generatedKey);
    }

    async getKeyForUser(user: UserModel): Promise<string>{
        const iterateOver = Object.keys(this.keyMap);

        for(let key of iterateOver) {
            if(this.keyMap[key].email === user.email){
                return Promise.resolve(key);
            }
        }

        return Promise.resolve(null);
    }

    async removeKey(key: string) {
        delete this.keyMap[key];
    }

    confirmKey(key: string): UserModel{
        const found = this.keyMap[key];
        return found;
    }

    async updateKey(user: UserModel, originalKey: string): Promise<string>{
        const current = this.confirmKey(originalKey);
        if(current !== null) {
            this.keyMap[originalKey] = user;
        }
        return Promise.resolve(this.keyMap[originalKey]);
    }

    async createRandomKey(): Promise<string> {
        const promise = new Promise<string>((resolve, reject) => {
            crypto.randomBytes(128, (err, buffer) => {
                const token = buffer.toString('hex');
                resolve(token);
            });
        });

        return promise;
    }
}

export default new AuthCache();