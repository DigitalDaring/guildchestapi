import DbAuth from '../dbauth/index';

class UserMiddleware {

    constructor(){

    }

    async optionalAuthenticated(req, res, next) {
        const authToken = req.headers.authorization;
        let user = null;
        if (authToken) {
            user = await DbAuth.confirmKey(authToken.toString());
        }
        req.user = user;
        next()
    }

    async requireAuthenticated(req, res, next){
        const authToken = req.headers.authorization;
        if (authToken) {
            const foundUser = await DbAuth.confirmKey(authToken.toString());

            if (foundUser !== null && foundUser !== undefined) {
                if(foundUser.confirmedDate !== null && foundUser.confirmedDate !== undefined)
                {
                    req.user = foundUser;
                    next();
                } else {
                    // haven't confirmed your account?
                    // forbidden!
                    res.status(403).send();
                }

            } else {
                res.status(401).send();
            }
        } else {
            res.status(401).send();
        }
    }

}

export default new UserMiddleware();