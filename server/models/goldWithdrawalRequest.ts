import {WithdrawalSignature} from "./withdrawalSignature";
import {ProfileModel} from "./profile";
import {ChestModel} from "./chest";

export class GoldWithdrawalRequest {
    _id: string;
    createdBy: ProfileModel;
    amount: number;
    chest: ChestModel;
    requiredSignatureCount: number;
    signatures: Array<WithdrawalSignature>;
}