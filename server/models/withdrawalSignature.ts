export class WithdrawalSignature {
    signeeName: string;
    signeeUserId: string;
    signedOn: Date;
}