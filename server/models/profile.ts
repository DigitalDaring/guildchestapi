import {KeyModel} from "./key";

export class ProfileModel {
    characterName: string;
    characterLevel: number;
    characterDescription: string;
    keys: Array<KeyModel>;
    searchCharacterName: string;
    avatarId: number;
}