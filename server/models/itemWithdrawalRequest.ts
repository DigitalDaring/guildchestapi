import {WithdrawalSignature} from "./withdrawalSignature";
import {ChestModel, StoredItem} from "./chest";
import {ProfileModel} from "./profile";

export class ItemWithdrawalRequest {
    createdBy: ProfileModel;
    items: Array<StoredItem>;
    chest: ChestModel;
    requiredSignatureCount: number;
    signatures: Array<WithdrawalSignature>;
}