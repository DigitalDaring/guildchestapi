import express from 'express';
import ProfileService from '../services/profile-service';
import UserMiddleware from '../middleware/user';
import {ProfileModel} from "../models/profile";

export class ProfilesRoutes {

    constructor(){}

    registerRoutes(router: express.Router): void {

        router.get('/profiles/search/:text', UserMiddleware.optionalAuthenticated, async(req, res) => {
            try {
                const foundProfiles = await ProfileService.search(req.params.text);
                res.status(200).json(foundProfiles);
            } catch (ex) {
                res.status(500).json(ex);
            }
        });

        router.get('/profiles/:characterName', UserMiddleware.optionalAuthenticated, async (req, res) => {
            try {
                const result = await ProfileService.find(req.params.characterName);

                const currentUser = req['user'];
                if(!currentUser ||
                    !currentUser.profile ||
                    currentUser.profile.searchCharacterName !== req.params.characterName.toLowerCase()) {
                    delete result.keys;
                }

                res.status(200).json(result);
            } catch (ex) {
                console.log(ex);
                res.status(500).json(ex);
            }
        });

        router.post('/profiles/update', UserMiddleware.requireAuthenticated, async (req, res) => {
            try {
                const updates = new ProfileModel();
                updates.characterName = req.body.characterName;
                updates.characterDescription = req.body.characterDescription;
                updates.characterLevel = req.body.characterLevel;
                updates.avatarId = req.body.avatarId;
                const cacheKey = req.headers.authorization as string;
                const result = await ProfileService.update(cacheKey, req['user']._id, updates);
                res.status(200).json(result);
            } catch (ex) {
                res.status(500).json(ex);
            }
        });

    }

}