import express from 'express';
import UserMiddleware from '../middleware/user';
import {ProfileModel} from "../models/profile";
import {ObjectID} from "bson";
import ChestService from '../services/chest-service';
import ProfileService from '../services/profile-service';
import {UserModel} from "../models/user";
import {GoldWithdrawalRequest} from "../models/goldWithdrawalRequest";
import WithdrawalService from '../services/withdrawal-service';

export class ChestsRoutes {

    constructor() {
    }

    registerRoutes(router: express.Router): void {

        router.get("/chests", UserMiddleware.requireAuthenticated, async (req, res) => {
            try {
                const currentUser = req["user"];
                const results = await ChestService.listChestsForUser(currentUser._id);
                res.status(200).json(results);
            } catch (error) {
                res.status(500).json(error);
            }
        });

        router.get("/chests/:id/details", UserMiddleware.requireAuthenticated, async (req, res) => {
            try {
                const currentUser = req["user"];
                const results = await ChestService.getChestDetails(req.params.id, currentUser);
                res.status(200).json(results);
            } catch (error) {
                res.status(500).json(error);
            }
        });

        router.post("/chests/new", UserMiddleware.requireAuthenticated, async(req, res) => {
           try {
               const currentUser = req["user"] as UserModel;
               if(!currentUser.profile || !currentUser.profile.characterName) {

               }
               const chestName = req.body.chestName;
               const username = currentUser.profile ? currentUser.profile.characterName : currentUser._id;
               const newChest =
                   await ChestService.createNewChest(currentUser._id, currentUser.profile.characterName, chestName);

               const updatedProfile =
                   await ProfileService.addChestKey(username, newChest);

               res.status(200).json(newChest);
           } catch(error) {
               res.status(500).json(error);
           }
        });

        router.post("/chests/:id/gold/deposit", UserMiddleware.requireAuthenticated, async(req, res) => {
            try {
                const currentUser = req["user"] as UserModel;
                const chestId = req.params.id;

                const toDeposit = parseFloat(req.body.deposit);
                const amount = Math.floor(toDeposit);

                await ChestService.addGoldToChest(amount, currentUser, chestId);

                const updatedChest = await ChestService.getChestDetails(chestId, currentUser);
                console.log('chest that changed: ', updatedChest);

                res.status(200).json(updatedChest);
            } catch(error) {
                res.status(500).json(error);
            }
        });

        router.post("/chests/:id/gold/withdraw", UserMiddleware.requireAuthenticated, async(req, res) => {
            try {
                const currentUser = req["user"] as UserModel;
                const chestId = req.params.id;
                const targetChest = await ChestService.getChestDetails(chestId, currentUser);

                const allKeysForChest = await ChestService.listProfilesWithKeysToChest(chestId);


                const toWithdraw = parseFloat(req.body.withdraw);
                const amount = Math.floor(toWithdraw);

                const withdrawalRequest = new GoldWithdrawalRequest();
                withdrawalRequest.amount = amount;
                withdrawalRequest.chest = targetChest;
                withdrawalRequest.requiredSignatureCount = 0;
                withdrawalRequest.signatures = [];
                withdrawalRequest.createdBy = currentUser.profile;

                if(allKeysForChest.length < 3) {
                    withdrawalRequest.requiredSignatureCount = 0;
                    const insertedRequest = await WithdrawalService.insertGoldWithdrawalRequest(withdrawalRequest);
                    const completedRequest = await WithdrawalService.completeGoldWithdrawalRequest(withdrawalRequest);
                    delete completedRequest.chest.log;
                    delete completedRequest.createdBy.keys;
                    res.status(200).json(completedRequest);
                } else if (allKeysForChest.length < 5) {
                    withdrawalRequest.requiredSignatureCount = 2;
                    const insertedRequest = await WithdrawalService.insertGoldWithdrawalRequest(withdrawalRequest);
                    res.status(200).json(insertedRequest);
                } else {
                    withdrawalRequest.requiredSignatureCount = 3;
                    const insertedRequest = await WithdrawalService.insertGoldWithdrawalRequest(withdrawalRequest);
                    res.status(200).json(insertedRequest);
                }


            } catch (error) {
                res.status(500).json(error);
            }
        });

        router.post("/chests/:id/item/deposit", UserMiddleware.requireAuthenticated, async(req, res) => {
            try {
                const currentUser = req["user"] as UserModel;
                const chestId = req.params.id;

                const itemName = req.body.item;
                const itemAmount = req.body.amount || 0;
                const amount = Math.floor(itemAmount);

                const updatedChest = await ChestService.addItemToChest(itemName, amount, currentUser, chestId);
                console.log('chest that changed: ', updatedChest);

                res.status(200).json(updatedChest);
            } catch(error) {
                res.status(500).json(error);
            }
        });

        router.post("/chests/:id/item/update", UserMiddleware.requireAuthenticated, async(req, res) => {
            try {
                const currentUser = req["user"] as UserModel;
                const chestId = req.params.id;

                const itemName = req.body.item;
                const itemAmount = req.body.amount || 0;
                const iconId = req.body.iconId || 0;
                const amount = Math.floor(itemAmount);

                const updatedChest = await ChestService.updateItemInChest(itemName, amount, iconId, currentUser, chestId);
                console.log('chest that changed: ', updatedChest);

                res.status(200).json(updatedChest);
            } catch(error) {
                res.status(500).json(error);
            }
        });

        router.post("/chests/:id/item/remove", UserMiddleware.requireAuthenticated, async(req, res) => {
           try {
               const currentUser = req["user"] as UserModel;
               const chestId = req.params.id;

               const itemName = req.body.item;
               const itemAmount = req.body.amount || 0;
               const amount = Math.floor(itemAmount);

               const updatedChest = await ChestService.removeItemFromChest(itemName, currentUser, chestId);
               console.log('removed item from: ', updatedChest);

               res.status(200).json(updatedChest);
           } catch (error) {
               res.status(500).json(error);
           }
        });

        router.get("/chests/:id/keys", UserMiddleware.requireAuthenticated, async(req, res) => {
            try {
                const profilesWithKeys = await ChestService.listProfilesWithKeysToChest(req.params.id);
                res.status(200).json(profilesWithKeys);
            } catch (ex) {
                res.status(500).json(ex);
            }
        });


    }
}