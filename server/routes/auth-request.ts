import {UserModel} from "../models/user";
import DbAuth from "../dbauth";
import express from 'express';
import UserService from '../services/user-service';
import UserMiddleware from "../middleware/user";

export class AuthRequestRoutes {

    private userService = UserService;

    constructor() {

    }

    registerRoutes(router: express.Router): void {
        router.post("/auth/login", async (req, res) => {
            try {
                const foundUser = await this.userService.login(req.body.email, req.body.password)
                const key = await DbAuth.registerNewKey(foundUser);
                delete foundUser.token;
                res.status(200).json({
                    user: foundUser,
                    key
                });
            } catch (e) {
                res.status(401).send();
            }

        });

        router.post("/auth/logout", UserMiddleware.requireAuthenticated, async(req, res) => {
            try{
                const foundUser = req['user'];
                const foundKey = await DbAuth.getKeyForUser(foundUser);
                await DbAuth.removeKey(foundKey);
                res.status(200).json({message: 'Successfully Logged Out'});
            } catch (e) {
                res.status(500).json(e);
            }
        });

        router.get("/auth/confirm/:email/:id", async(req, res) => {
           try{
               const foundUser = await this.userService.validateUser(req.params.email, req.params.id);

               res.status(200).json({confirmedDate: foundUser.confirmedDate});
           } catch (e) {
               console.log(e);
               res.status(500).json(e);
           }

        });
    }

}