import express from 'express';
import UserMiddleware from '../middleware/user';
import {ProfileModel} from "../models/profile";
import {ObjectID} from "bson";

export class ClientsRoutes {

    constructor(){}

    registerRoutes(router: express.Router): void {

        // router.get("/clients", UserMiddleware.requireAuthenticated, async (req, res) => {
        //     try {
        //         const results = await ClientsService.listClientCompanies();
        //         res.status(200).json(results);
        //     } catch (error) {
        //         res.status(500).json(error);
        //     }
        // });
        //
        // router.get("/client/:id", UserMiddleware.requireAuthenticated, async(req, res) => {
        //     try {
        //         const targetId = new ObjectID(req.params.id);
        //         const result = await ClientsService.getClientCompany(targetId);
        //         res.status(200).json(result);
        //     } catch (error) {
        //         res.status(500).json(error);
        //     }
        // });
        //
        // router.post("/client", UserMiddleware.requireAuthenticated, async(req, res) => {
        //     try {
        //
        //         const createdBy = req["user"].profile;
        //         const marketeersArray = createdBy ? [createdBy] : [];
        //
        //
        //         const toCreate = {
        //             name: req.body.name,
        //             description: req.body.description,
        //             projects: [],
        //             contacts: [],
        //             marketeers: marketeersArray
        //         } as ClientCompanyModel;
        //
        //         const result = await ClientsService.createClientCompany(toCreate);
        //         res.status(200).json(result);
        //     } catch(error) {
        //         res.status(500).json(error);
        //     }
        // });
        //
        // router.post("/client/:id", UserMiddleware.requireAuthenticated, async(req, res) => {
        //    try {
        //
        //        const targetId = new ObjectID(req.params.id);
        //
        //        const toUpdate = {
        //            name: req.body.name,
        //            description: req.body.description,
        //            projects: req.body.projects,
        //            contacts: req.body.contacts,
        //            marketeers: req.body.marketeers
        //        } as ClientCompanyModel;
        //
        //        const result = await ClientsService.updateClientCompany(targetId, toUpdate);
        //        res.status(200).json(result);
        //    } catch (error) {
        //        res.status(500).json(error);
        //    }
        // });
        //
        // router.delete("/client/:id", UserMiddleware.requireAuthenticated, async(req, res) => {
        //     try {
        //         const targetId = new ObjectID(req.params.id);
        //         const result = await ClientsService.disableClientCompany(targetId);
        //         res.status(200).json(result);
        //     } catch(error) {
        //         res.status(500).json(error);
        //     }
        // })
    }

}