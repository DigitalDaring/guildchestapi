import {UserModel} from "../models/user";

const sgMail = require("@sendgrid/mail");

class EmailService {

    sendgridKey: string;
    signupConfirmationUrl: string;

    constructor() {
        this.sendgridKey = process.env.SENDGRID_KEY || '';
        this.signupConfirmationUrl = process.env.SIGNUP_CONFIRMATION_URL || '';
        sgMail.setApiKey(this.sendgridKey);
    }

    async sendValidationEmail(user: UserModel): Promise<void> {
        const promise = new Promise<void>(async (resolve, reject) => {

            const responseUrl = `${this.signupConfirmationUrl}/${user.confirmationCode}`;

            try{

                const msg = {
                    to: user.email,
                    from: 'support@guildchest.com',
                    subject: 'Confirm Sign-up for GuildChest',
                    text: `We recently received a sign-up request for this email address.
                    To confirm your sign-up, please visit ${responseUrl}  If you did not
                    request an account, you don't have to do anything!`,
                    html: `We recently received a sign-up request for this email address.
                    To confirm your sign-up, please enter the code 
                    <div style="font-size: 24px; padding: 10px; background-color: #333; color: #CCC;">
                        ${user.confirmationCode}
                    </div>
                    in the app!
                    If you did not request an account, you don't have to do anything!`
                };

                const result = await sgMail.send(msg);
                resolve();
            } catch (error) {
                reject(error);
            }
        });

        return promise;
    }

}

export default new EmailService();