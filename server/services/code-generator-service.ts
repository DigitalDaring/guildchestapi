const allCharacters = ['a','b','c','d','e','f','g','h',
    'k','m','n','p','q','r',
    't','u','v','w','x','y','z','2',
    '3', '4', '5', '6', '7', '8', '9'];

class CodeGeneratorService {

    generateCharacter() {
        const randomIndex = Math.floor(Math.random() * allCharacters.length);
        return allCharacters[randomIndex];
    }

    generateCode(length: Number) {
        let returnMe = '';

        for(var i = 0; i < length; i++) {
            returnMe += this.generateCharacter();
        }

        return returnMe;
    }
}

export default new CodeGeneratorService();