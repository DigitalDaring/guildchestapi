// import DbService from './db-service';
// import {ClientCompanyModel} from "../models/client-company";
// import {ObjectID} from "bson";
//
// class ClientService {
//
//     async listClientCompanies(): Promise<Array<ClientCompanyModel>>{
//         const promise = new Promise<Array<ClientCompanyModel>>(async(resolve, reject) => {
//            try {
//                const allClients = await DbService.db.collection("clients")
//                    .find({$or: [{deleted: false},{deleted: null}]}).map(x => x as ClientCompanyModel).toArray();
//
//                resolve(allClients);
//            } catch (error) {
//                 reject(error);
//            }
//         });
//
//         return promise;
//     }
//
//     async getClientCompany(id: ObjectID): Promise<ClientCompanyModel> {
//         const promise = new Promise<ClientCompanyModel>(async(resolve, reject) => {
//            try {
//                const thisClient = await DbService.db.collection("clients")
//                    .findOne({_id: id}) as ClientCompanyModel;
//
//                resolve(thisClient);
//            } catch (error) {
//                reject(error);
//            }
//         });
//
//         return promise;
//     }
//
//     async createClientCompany(toCreate: ClientCompanyModel): Promise<ObjectID> {
//         const promise = new Promise<ObjectID>(async(resolve, reject) => {
//
//             try{
//                 const existing = await DbService.db.collection("clients").findOne({
//                     name: toCreate.name
//                 });
//
//                 if(existing){
//                     reject({message: "A company with that name already exists!"});
//                 } else {
//                     const inserted = await DbService.db.collection("clients").insertOne(toCreate);
//
//                     resolve(inserted.insertedId);
//                 }
//
//             } catch(error){
//                 reject(error);
//             }
//
//         });
//
//         return promise;
//     }
//
//     async updateClientCompany(id: ObjectID, updates: ClientCompanyModel): Promise<any> {
//         const promise = new Promise(async(resolve, reject) => {
//
//             try {
//                 const updated = await DbService.db.collection("clients").updateOne({
//                     _id: id
//                 }, {
//                     $set: updates
//                 });
//
//                 resolve();
//             } catch (error) {
//                 reject(error);
//             }
//
//         });
//
//         return promise;
//     }
//
//     async disableClientCompany(id: ObjectID): Promise<any> {
//         try {
//             const result = await DbService.db.collection("clients").updateOne({
//                 _id: id
//             }, {
//                 $set: {
//                     deleted: true
//                 }
//             });
//
//             return Promise.resolve(result);
//         } catch (error) {
//             return Promise.reject(error);
//         }
//     }
//
// }
//
// export default new ClientService();