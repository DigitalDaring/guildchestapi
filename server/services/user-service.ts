import {UserModel} from "../models/user";
import EncryptionService from './encryption-service';
import DbService from './db-service';
import {v4 as uuid} from 'uuid';
import {MongoClient, Db} from "mongodb";
import EmailService from './email-service';
import {ProfileModel} from "../models/profile";
import {config} from "../config";
const ObjectId = require('mongodb').ObjectID;
import CodeGeneratorService from './code-generator-service';
import {ObjectID} from "bson";

class UserService {

    constructor() {

    }

    async login(email: string, password: string): Promise<UserModel> {
        const promise = new Promise<UserModel>(async(resolve, reject) => {

            let conn = null;

            try{
                conn = await DbService.connectToDB(config.dbUri);
                const foundUser = await conn.db.collection("users").findOne({
                    email
                });

                if(foundUser){
                    const isValidPassword =
                        await EncryptionService.comparePassword(password, foundUser.safeword);

                    if(isValidPassword) {
                        delete foundUser.safeword;
                        resolve(foundUser);
                    } else {
                        reject({message: "Invalid Username or Password"});
                    }
                } else {
                    reject({message: "Invalid Username or Password"});
                }
            } catch(error) {
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }

    async findUser(userId: string): Promise<UserModel> {
        const promise = new Promise<UserModel>(async(resolve, reject) => {

            let conn = null;

            try{
                conn = await DbService.connectToDB(config.dbUri);
                const foundUser = await conn.db.collection("users").findOne({
                    _id: ObjectId(userId)
                });

                resolve(foundUser);
            } catch(error) {
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }


    async signup(email: string, password: string): Promise<ObjectID> {
        const promise = new Promise<ObjectID>(async (resolve, reject) => {

            let conn = null;

            try {
                conn = await DbService.connectToDB(config.dbUri);
                const existing = await conn.db.collection("users").findOne({
                    email
                });

                if(existing){
                    reject({message: "User already exists"})
                }

                const hashedPassword = await EncryptionService.encryptPassword(password);

                const confirmationCode = CodeGeneratorService.generateCode(5);

                const toCreate = {
                    email,
                    safeword: hashedPassword,
                    signUpDate: new Date(),
                    confirmedDate: null,
                    confirmationCode: confirmationCode,
                    profile: new ProfileModel()
                } as UserModel;


                const inserted = await conn.db.collection("users").insertOne(toCreate);

                const result = await conn.db.collection("users").findOne({
                    email
                });
                const sentEmail = await EmailService.sendValidationEmail(result);
                resolve(inserted.insertedId);

            } catch (error) {
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }

    async validateUser(email: string, confirmationCode: string): Promise<UserModel> {
        const promise = new Promise<UserModel>(async (resolve, reject) => {
            let conn = null;
            try {
                conn = await DbService.connectToDB(config.dbUri);
                const relatedUser = await conn.db.collection("users").findOne({
                    email,
                    confirmationCode
                });

                if(!relatedUser){
                    reject({message: "unable to find user matching this confirmation code"});
                }

                if(relatedUser.confirmedDate !== null){
                    reject({message: "User has already confirmed sign-up!"});
                }

                const confirmDate = new Date();

                const update = await conn.db.collection("users").updateOne({
                    email,
                    confirmationCode
                }, {
                    "$set": {
                        "confirmedDate": confirmDate
                    }
                });

                relatedUser.confirmedDate = confirmDate;

                console.log(`updated ${update} records!`);

                resolve(relatedUser);
            } catch (error) {
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }
}

export default new UserService();