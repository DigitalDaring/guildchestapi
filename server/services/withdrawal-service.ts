import DbService from './db-service';
import {ChestModel, ChestLogEvent} from "../models/chest";
import {config} from "../config";
const ObjectId = require('mongodb').ObjectID;
import {UserModel} from '../models/user';
import {v4} from 'uuid';
import {GoldWithdrawalRequest} from "../models/goldWithdrawalRequest";

class WithdrawalService {

    async insertGoldWithdrawalRequest(withdrawalRequest: GoldWithdrawalRequest): Promise<GoldWithdrawalRequest> {

        const promise = new Promise<GoldWithdrawalRequest>(async(resolve, reject) => {
            let conn = null;

            try {

                conn = await DbService.connectToDB(config.dbUri);
                const result = await conn.db.collection('withdrawals').insertOne(withdrawalRequest);
                const createdRequest = result.ops[0];

                resolve(createdRequest);

            } catch (error) {
                console.log(error);
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }

    async completeGoldWithdrawalRequest(request: GoldWithdrawalRequest): Promise<GoldWithdrawalRequest> {

        const promise = new Promise<GoldWithdrawalRequest>(async(resolve, reject) => {
            let conn = null;

            try {

                conn = await DbService.connectToDB(config.dbUri);
                const result = await conn.db.collection('withdrawals').updateOne(
                    {
                        _id: ObjectId(request._id)
                    },
                    {
                        $set: {
                            completeDate: new Date()
                        }
                    }
                );

                await this.withdrawGoldFromChest(request);

                const updatedRequest = await conn.db.collection('withdrawals')
                    .findOne({_id: ObjectId(request._id)});

                resolve(updatedRequest);

            } catch (error) {
                console.log(error);
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }

    async withdrawGoldFromChest(withdrawalRequest: GoldWithdrawalRequest): Promise<ChestModel> {

        console.log('trying to remove ', withdrawalRequest.amount);
        const promise = new Promise<ChestModel>(async(resolve, reject) => {
            let conn = null;

            try {

                conn = await DbService.connectToDB(config.dbUri);
                let existingChest = await conn.db.collection('chests')
                    .findOne({_id: ObjectId(withdrawalRequest.chest._id)}) as ChestModel;

                if(!existingChest) {
                    throw "An error occurred while looking for your chest!";
                }

                if(!withdrawalRequest.createdBy.keys.some(key => key.code.toString() === existingChest._id.toString())) {
                    throw "You don't have a key for that chest!";
                }

                const existingAmount = existingChest.gold || 0;
                if(existingAmount < withdrawalRequest.amount) {
                    throw "There isn't enough gold in the chest for that!";
                }

                if(withdrawalRequest.signatures.length < withdrawalRequest.requiredSignatureCount){
                    throw "Not enough signatures to withdraw your gold yet!";
                }

                const signatures = withdrawalRequest.signatures.map(x => x.signeeName).join(', ');
                let description = `${withdrawalRequest.createdBy.characterName} withdrew ${withdrawalRequest.amount} gold`;
                if(withdrawalRequest.signatures.length > 0){
                    description += ` with approval from ${signatures}`;
                }

                const goldRemoveLog = new ChestLogEvent();
                goldRemoveLog.occurredOn = new Date();
                goldRemoveLog.description = description;
                goldRemoveLog._id = v4();

                const newAmount = existingAmount - withdrawalRequest.amount;

                const result = await conn.db.collection('chests').updateOne(
                    {_id: ObjectId(withdrawalRequest.chest._id)},
                    {
                        "$set": {
                            gold: newAmount
                        },
                        "$push": {
                            log: goldRemoveLog
                        }
                    }
                );

                const updatedChest = await conn.db.collection('chests').findOne({_id: ObjectId(withdrawalRequest.chest._id)});

                resolve(updatedChest);

            } catch (error) {
                console.log(error);
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }
}

export default new WithdrawalService();