import DbService from './db-service';
import {ChestModel, ChestLogEvent, StoredItem} from "../models/chest";
import {config} from "../config";
const ObjectId = require('mongodb').ObjectID;
import {UserModel} from '../models/user';
import {v4} from 'uuid';
import {GoldWithdrawalRequest} from "../models/goldWithdrawalRequest";
import {ProfileModel} from '../models/profile';


class ChestService {

    async listChestsForUser(userId: string): Promise<Array<ChestModel>>{
        const promise = new Promise<Array<ChestModel>>(async(resolve, reject) => {
            let conn = null;
            try {
               let foundChests = [];
               conn = await DbService.connectToDB(config.dbUri);
               console.log('about to ask for user', ObjectId(userId));
               const thisUser = await conn.db.collection('users').findOne({_id: ObjectId(userId)});
               console.log('got user: ', thisUser);
               if(thisUser.profile && thisUser.profile.keys){
                   const keyCodes = thisUser.profile.keys.map(key => key.code);
                   foundChests = await conn.db.collection("chests")
                       .find({_id: {$in: keyCodes}}).map(x => x as ChestModel).toArray();
               }

               resolve(foundChests);
            } catch (error) {
                console.log(error);
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }

    async createNewChest(createdByUserId: string, createdByName: string, chestName: string): Promise<ChestModel> {
        const promise = new Promise<ChestModel>(async(resolve, reject) => {
            let conn = null;

            try {

                conn = await DbService.connectToDB(config.dbUri);
                const chestAlreadyExists = await conn.db.collection('chests').findOne({name: chestName});

                if(chestAlreadyExists) {
                    throw "A chest with that name already exists somewhere, hidden in shadow";
                }

                const newChest = new ChestModel();
                newChest.contents = [];
                newChest.gold = 0;
                newChest.name = chestName;

                const creationLog = new ChestLogEvent();
                creationLog.occurredOn = new Date();
                creationLog.description = "Created by " + createdByName;
                creationLog._id = v4();

                newChest.log = [creationLog];

                const result = await conn.db.collection('chests').insertOne(newChest);
                const createdChest = result.ops[0];

                resolve(createdChest);

            } catch (error) {
                console.log(error);
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }

    async listProfilesWithKeysToChest(chestId: string): Promise<Array<ProfileModel>> {
        const promise = new Promise<Array<ProfileModel>>(async(resolve, reject) => {
            let conn = null;
            try {
                conn = await DbService.connectToDB(config.dbUri);
                const foundUsers = await conn.db.collection('users').find({
                    'profile.keys': {
                        $elemMatch: {code: {$eq: ObjectId(chestId)}}
                    }
                }).toArray();
                const foundProfiles = foundUsers.map(x => x.profile);
                foundProfiles.forEach(profile => {
                    delete profile.keys;
                });

                resolve(foundProfiles);
            } catch (error) {
                console.log(error);
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }


    async addGoldToChest(amount: number, addedBy: UserModel, addToChestID: string): Promise<ChestModel> {

        const promise = new Promise<ChestModel>(async(resolve, reject) => {
            let conn = null;

            try {

                conn = await DbService.connectToDB(config.dbUri);
                let existingChest = await conn.db.collection('chests')
                    .findOne({_id: ObjectId(addToChestID)}) as ChestModel;

                if(!existingChest) {
                    throw "An error occurred while looking for your chest!";
                }

                if(!addedBy.profile.keys.some(key => key.code.toString() === existingChest._id.toString())) {
                    throw "You don't have a key for that chest!";
                }

                const goldAddLog = new ChestLogEvent();
                goldAddLog.occurredOn = new Date();
                goldAddLog.description = `${addedBy.profile.characterName} added ${amount} gold`;
                goldAddLog._id = v4();

                const existingAmount = existingChest.gold || 0;

                const newAmount = existingAmount + amount;

                const result = await conn.db.collection('chests').updateOne(
                    {_id: ObjectId(addToChestID)},
                    {
                        "$set": {
                            gold: newAmount
                        },
                        "$push": {
                            log: goldAddLog
                        }
                    }
                );

                const updatedChest = result;

                resolve(updatedChest);

            } catch (error) {
                console.log(error);
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }

    async updateItemInChest(item: string, quantity: number, iconId: number, addedBy: UserModel, inChestId: string) : Promise<ChestModel> {
        console.log("updating item: ", item);

        const promise = new Promise<ChestModel>(async(resolve, reject) => {
            // TODO: implement actual item update functionality
        });

        return promise;
    }

    async addItemToChest(item: string, quantity: number, addedBy: UserModel, addToChestId: string) : Promise<ChestModel> {
        console.log("adding item: ", item);

        const promise = new Promise<ChestModel>(async(resolve, reject) => {
            let conn = null;

            try {
                conn = await DbService.connectToDB(config.dbUri);
                const existingChest = await conn.db.collection('chests')
                    .findOne({_id: ObjectId(addToChestId)}) as ChestModel;

                if(!existingChest) {
                    throw "An error occurred while looking for your chest!";
                }

                if(!addedBy.profile.keys.some(key => key.code.toString() === existingChest._id.toString())) {
                    throw "You don't have a key for that chest!";
                }

                const existingItems = existingChest.contents
                    .find(storedItem => {return storedItem.searchableName === item.toLowerCase()});
                if (existingItems) {
                    const newAmount = (existingItems.count || 0) + quantity;

                    const result = await conn.db.collection('chests').updateOne(
                        {_id: ObjectId(addToChestId), "contents.searchableName": item.toLowerCase()},
                        {
                            $set: {"contents.$.count": newAmount}
                        }
                    )
                } else {

                    const newItem = new StoredItem();
                    newItem.appraisedValue = 0;
                    newItem.count = quantity;
                    newItem.name = item;
                    newItem.searchableName = item.toLowerCase();

                    const result = await conn.db.collection('chests').updateOne(
                        {_id: ObjectId(addToChestId)},
                        {
                            $push: {"contents": newItem}
                        }
                    )
                }

                const itemAddLog = new ChestLogEvent();
                itemAddLog.occurredOn = new Date();
                itemAddLog.description = `${addedBy.profile.characterName} added ${quantity} x ${item}`;
                itemAddLog._id = v4();

                const updatedChest = await this.getChestDetails(addToChestId, addedBy);

                resolve(updatedChest);
            } catch (ex) {
                console.log(ex);
                reject(ex);
            }
        });

        return promise;

    }

    async removeItemFromChest(item: string, removedBy: UserModel, chestId: string) : Promise<ChestModel> {
        console.log("destroying item: ", item);

        const promise = new Promise<ChestModel>(async(resolve, reject) => {
            let conn = null;

            try {
                conn = await DbService.connectToDB(config.dbUri);
                const existingChest = await conn.db.collection('chests')
                    .findOne({_id: ObjectId(chestId)}) as ChestModel;

                if(!existingChest) {
                    throw "An error occurred while looking for your chest!";
                }

                if(!removedBy.profile.keys.some(key => key.code.toString() === existingChest._id.toString())) {
                    throw "You don't have a key for that chest!";
                }

                const existingItems = existingChest.contents
                    .find(storedItem => {return storedItem.searchableName === item.toLowerCase()});

                if (existingItems && existingItems.count > 1) {
                    const newAmount = existingItems.count - 1;

                    const result = await conn.db.collection("chests").updateOne(
                        {_id: ObjectId(chestId), "contents.searchableName": item.toLowerCase()},
                        {
                            $set: {"contents.$.count": newAmount}
                        }
                    )
                } else if (existingItems && existingItems.count === 1) {
                    const result = await conn.db.collection("chests").updateOne(
                        {_id: ObjectId(chestId)},
                        {
                            $pull: {"contents": { searchableName: item.toLowerCase() } }
                        }
                    )
                } else {
                    throw "Unable to find that item, perhaps it has been stolen by a rogue?"
                }

                const itemAddLog = new ChestLogEvent();
                itemAddLog.occurredOn = new Date();
                itemAddLog.description = `${removedBy.profile.characterName} removed ${item}`;
                itemAddLog._id = v4();

                const updatedChest = await this.getChestDetails(chestId, removedBy);

                resolve(updatedChest);
            } catch (ex) {
                console.log(ex);
                reject(ex);
            }
        });

        return promise;

    }

    async getChestDetails(chestId: string, forUser: UserModel): Promise<ChestModel> {
        const promise = new Promise<ChestModel>(async(resolve, reject) => {
            let conn = null;

            try {

                conn = await DbService.connectToDB(config.dbUri);
                const existingChest = await conn.db.collection('chests')
                    .findOne({_id: ObjectId(chestId)}) as ChestModel;

                if(!existingChest) {
                    throw "An error occurred while looking for your chest!";
                }

                if(!forUser.profile.keys.some(key => key.code.toString() === existingChest._id.toString())) {
                    throw "You don't have a key for that chest!";
                }

                resolve(existingChest);

            } catch (error) {
                console.log(error);
                reject(error);
            } finally {
                if(conn && conn.client) {
                    conn.client.close();
                }
            }
        });

        return promise;
    }

}

export default new ChestService();