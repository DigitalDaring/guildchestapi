const path = require("path");

module.exports = [
    {
        entry: {
            "dist": "./server/index.ts"
        },
        devtool: "inline-source-map",
        node: {
        },
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    use: 'ts-loader',
                    exclude: /node_modules/
                }
            ]
        },
        target: "node",
        resolve: {
            extensions: ['.ts', '.js']
        },
        output: {
            filename: '[name]/index.js',
            path: path.resolve(__dirname, '')
        }
    }];
